using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LobbyText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_lobbyCodeText;
    [SerializeField] private Button m_readyButton;
    [SerializeField] private Button m_startButton;
    [SerializeField] private Button m_backButton;

    private void OnEnable()
    {
        m_readyButton.onClick.AddListener(OnReadyPressed);
        m_backButton.onClick.AddListener(OnBackPressed);

        if (GameLobbyManager.Instance.IsHost)
        {
            m_startButton.onClick.AddListener(OnStartButtonClicked);
            Events.LobbyEvents.OnLobbyReady += OnLobbyReady;        
        }
    }

    private void OnDisable()
    {
        m_readyButton.onClick.RemoveAllListeners();
        m_startButton.onClick.RemoveAllListeners();
        Events.LobbyEvents.OnLobbyReady -= OnLobbyReady;
    }

    public void Start()
    {
        m_lobbyCodeText.text = $"Lobby code: {GameLobbyManager.Instance.GetLobbyCode()}";
    }

    private async void OnReadyPressed()
    {
        bool succeed = await GameLobbyManager.Instance.SetPlayerReady();
        if (succeed)
        {
            m_readyButton.gameObject.SetActive(false);
        }
    }

    private void OnLobbyReady()
    {
        if (GameLobbyManager.Instance.IsHost)
        {
            m_startButton.gameObject.SetActive(true);
        }
    }

    private async void OnStartButtonClicked()
    {
        await GameLobbyManager.Instance.StartGame();
    }

    private void OnBackPressed()
    {
        SceneManager.LoadSceneAsync("Menu");
    }

}
