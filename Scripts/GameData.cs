using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class GameData : MonoBehaviour
{

    [Header("Characters List")]
    [SerializeField] private List<Character> characters = new List<Character>();
    
    public int selectedCharacter = 0;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void UpdateCharacter(int index)
    {
        selectedCharacter = index;
    }
}
