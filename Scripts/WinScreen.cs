using Unity.Netcode;
using TMPro;
using UnityEngine;

public class WinScreen : NetworkBehaviour
{
    private NetworkVariable<int> winner = new NetworkVariable<int>();

    [ServerRpc(RequireOwnership = false)]
    public void SetWinnerServerRpc(int update)
    {        
        winner.Value = update;
    }

    private void OnEnable()
    {
        winner.OnValueChanged += OnWinnerChanged;
    }

    private void OnDisable()
    {
        winner.OnValueChanged -= OnWinnerChanged;
    }

    private void OnWinnerChanged(int oldWinner, int newWinner)
    {
        Debug.Log("Changed");
        GetComponent<CanvasRenderer>().SetAlpha(1);
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);
        Debug.Log(gameObject.activeInHierarchy);
        if (winner.Value == 0)
        {
            transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Draw!";
            return;
        }
        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Player " + (winner.Value).ToString() + " Wins!";
    }
}
