using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{

    public class LobbySpawner : MonoBehaviour
    {
        [SerializeField] private List<LobbyPlayer> m_Players;

        private void OnEnable()
        {
            LobbyEvents.OnLobbyUpdated += OnLobbyUpdated;
        }

        private void OnDisable()
        {
            LobbyEvents.OnLobbyUpdated -= OnLobbyUpdated;
        }

        private void OnLobbyUpdated()
        {
            List<LobbyPlayerData> playerDatas = GameLobbyManager.Instance.GetPlayers();

            for (int i = 0; i < playerDatas.Count; i++)
            {
                LobbyPlayerData data = playerDatas[i];
                m_Players[i].SetData(data);
            }
        }
    }
}
