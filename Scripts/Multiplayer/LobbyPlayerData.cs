using System.Collections.Generic;
using Unity.Services.Lobbies.Models;

public class LobbyPlayerData
{
    private string m_Id;
    private string m_Gamertag;
    private bool m_IsReady;

    public string Id => m_Id;
    public string Gamertag => m_Gamertag;
    public bool IsReady
    {
        get => m_IsReady;
        set => m_IsReady = value;
    }

    public void Initialize(string id, string gamertag)
    {
        m_Id = id;
        m_Gamertag = gamertag;
    }

    public void Initialize(Dictionary<string, PlayerDataObject> playerData)
    {
        UpdateState(playerData);
    }

    public void UpdateState(Dictionary<string, PlayerDataObject> playerData)
    {
        if (playerData.ContainsKey("Id"))
        {
            m_Id = playerData["Id"].Value;
        }

        if (playerData.ContainsKey("Gamertag"))
        {
            m_Gamertag = playerData["Gamertag"].Value;
        }

        if (playerData.ContainsKey("IsReady"))
        {
            m_IsReady = playerData["IsReady"].Value == "True";
        }
    }

    public Dictionary<string, string> Serialize()
    {
        return new Dictionary<string, string>()
        {
            {"Id", m_Id },
            {"Gamertag", m_Gamertag },
            {"IsReady", m_IsReady.ToString() }
        };
    }
}
