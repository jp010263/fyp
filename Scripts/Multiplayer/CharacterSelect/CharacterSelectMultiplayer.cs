using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class CharacterSelectMultiplayer : NetworkBehaviour
{
    [SerializeField] private NetworkObject[] playerSlots;
    [SerializeField] private NetworkObject[] cursorPrefab;
    [SerializeField] private NetworkObject[] tokenPrefab;

    public override void OnNetworkSpawn()
    {    
        SpawnCursorTokenServerRpc();
        base.OnNetworkSpawn();
    }

    //Spawn tokens/cursors for character select
    [ServerRpc(RequireOwnership = false)]
    public void SpawnCursorTokenServerRpc()
    {
        int idNum = NetworkManager.Singleton.ConnectedClientsIds.Count - 1;
        ulong clientId = NetworkManager.Singleton.ConnectedClientsIds[idNum];    //Get current player ID

        NetworkObject playerSlot = Instantiate(playerSlots[idNum]);
        NetworkObject token = Instantiate(tokenPrefab[idNum]);
        NetworkObject cursor = Instantiate(cursorPrefab[idNum]);
        playerSlot.SpawnWithOwnership(clientId);        
        cursor.SpawnWithOwnership(clientId);      
        token.SpawnWithOwnership(clientId); //Token after for UI
        /*
        playerSlot.DestroyWithScene = true;
        cursor.DestroyWithScene = true;
        token.DestroyWithScene = true;
        */
    }
}
