using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Unity.Netcode;

public class CharacterGridMultiplayer : NetworkBehaviour
{
    private GridLayoutGroup gridLayout;
    public Vector2 slotArtworkSize = new Vector2(300, 300);

    public static CharacterGridMultiplayer instance;
    [Header("Characters List")]
    [SerializeField] private List<Character> characters = new List<Character>();
    [Space]
    [Header("Public References")]
    [SerializeField] private GameObject charCellPrefab;
    [SerializeField] private GameObject gridBgPrefab;
    [SerializeField] private Transform playerSlotsContainer;

    public Character confirmedCharacter;

    private int clientId;
    private GameData gd;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        gd = GameObject.FindWithTag("GameData").GetComponent<GameData>();
        gridLayout = GetComponent<GridLayoutGroup>();
        GetComponent<RectTransform>().sizeDelta = new Vector2(gridLayout.cellSize.x * 4, gridLayout.cellSize.y * 2);
        RectTransform gridBG = Instantiate(gridBgPrefab, transform.parent).GetComponent<RectTransform>();
        gridBG.transform.SetSiblingIndex(transform.GetSiblingIndex());
        gridBG.sizeDelta = GetComponent<RectTransform>().sizeDelta;      
        foreach (Character character in characters)
        {
            SpawnCharacterCell(character);
        }
    }

    public List<Character> Characters()
    {
        return characters;
    }

    private void SpawnCharacterCell(Character character)
    {
        GameObject charCell = Instantiate(charCellPrefab, transform);

        charCell.name = character.characterName;

        Image artwork = charCell.transform.Find("artwork").GetComponent<Image>();
        TextMeshProUGUI name = charCell.transform.Find("nameRect").GetComponentInChildren<TextMeshProUGUI>();

        artwork.sprite = character.characterSprite;
        name.text = character.characterName;

        artwork.GetComponent<RectTransform>().pivot = uiPivot(artwork.sprite);
        artwork.GetComponent<RectTransform>().sizeDelta *= character.zoom;
    }

    public void ShowCharacterInSlot(int index)
    {
        clientId = (int)NetworkManager.Singleton.LocalClientId;
        if (IsOwner)
        {
            ShowCharacterInSlotClientRpc(index);
        }
        else
        {
            ShowCharacterInSlotServerRpc(index);
        }


        Character character = null;
        if (index != -1)
        {
            character = characters[index];
        }

        bool nullChar = (character == null);

        Color alpha = nullChar ? Color.clear : Color.white;
        Sprite artwork = nullChar ? null : character.characterSprite;
        string name = nullChar ? string.Empty : character.characterName;
        /*
        string playernickname = "Player " + (clientId + 1).ToString();
        string playernumber = "P" + (clientId + 1).ToString();
        */

        Transform slot = null;
        foreach (Transform slots in playerSlotsContainer)
        {
            if (slots.GetComponent<NetworkObject>().IsOwner)
            {
                slot = slots;
            }
        }

        Transform slotArtwork = slot.Find("artwork");        
        
        Sequence s = DOTween.Sequence();
        s.Append(slotArtwork.DOLocalMoveX(-300, .05f).SetEase(Ease.OutCubic));
        s.AppendCallback(() => slotArtwork.GetComponent<Image>().sprite = artwork);
        s.AppendCallback(() => slotArtwork.GetComponent<Image>().color = alpha);
        s.Append(slotArtwork.DOLocalMoveX(300, 0));
        s.Append(slotArtwork.DOLocalMoveX(0, .05f).SetEase(Ease.OutCubic));
        
        slot.Find("name").GetComponent<TextMeshProUGUI>().text = name;
        //slot.Find("player").GetComponentInChildren<TextMeshProUGUI>().text = playernickname;
        //slot.Find("iconAndPx").GetComponentInChildren<TextMeshProUGUI>().text = playernumber;

    }

    [ServerRpc (RequireOwnership = false)]
    private void ShowCharacterInSlotServerRpc(int index, ServerRpcParams serverRpcParams = default)
    {
        Character character = null;
        if (index != -1)
        {
            character = characters[index];
        }

        bool nullChar = (character == null);

        Color alpha = nullChar ? Color.clear : Color.white;
        Sprite artwork = nullChar ? null : character.characterSprite;
        string name = nullChar ? string.Empty : character.characterName;
        /*
        string playernickname = "Player " + (clientId + 1).ToString();
        string playernumber = "P" + (clientId + 1).ToString();
        */

        Transform slot = null;
        foreach (Transform slots in playerSlotsContainer)
        {
            if (slots.GetComponent<NetworkObject>().OwnerClientId == serverRpcParams.Receive.SenderClientId)
            {
                slot = slots;
            }
        }

        Transform slotArtwork = slot.Find("artwork");

        Sequence s = DOTween.Sequence();
        s.Append(slotArtwork.DOLocalMoveX(-300, .05f).SetEase(Ease.OutCubic));
        s.AppendCallback(() => slotArtwork.GetComponent<Image>().sprite = artwork);
        s.AppendCallback(() => slotArtwork.GetComponent<Image>().color = alpha);
        s.Append(slotArtwork.DOLocalMoveX(300, 0));
        s.Append(slotArtwork.DOLocalMoveX(0, .05f).SetEase(Ease.OutCubic));

        slot.Find("name").GetComponent<TextMeshProUGUI>().text = name;
        /*
        slot.Find("player").GetComponentInChildren<TextMeshProUGUI>().text = playernickname;
        slot.Find("iconAndPx").GetComponentInChildren<TextMeshProUGUI>().text = playernumber;
        */
    }

    [ClientRpc]
    private void ShowCharacterInSlotClientRpc(int index)
    {
        Character character = null;
        if (index != -1)
        {
            character = characters[index];
        }

        bool nullChar = (character == null);

        Color alpha = nullChar ? Color.clear : Color.white;
        Sprite artwork = nullChar ? null : character.characterSprite;
        string name = nullChar ? string.Empty : character.characterName;
        /*
        string playernickname = "Player " + (clientId + 1).ToString();
        string playernumber = "P" + (clientId + 1).ToString();
        */

        Transform slot = null;
        foreach (Transform slots in playerSlotsContainer)
        {
            if (slots.GetComponent<NetworkObject>().IsOwnedByServer)
            {
                slot = slots;
            }
        }

        Transform slotArtwork = slot.Find("artwork");

        Sequence s = DOTween.Sequence();
        s.Append(slotArtwork.DOLocalMoveX(-300, .05f).SetEase(Ease.OutCubic));
        s.AppendCallback(() => slotArtwork.GetComponent<Image>().sprite = artwork);
        s.AppendCallback(() => slotArtwork.GetComponent<Image>().color = alpha);
        s.Append(slotArtwork.DOLocalMoveX(300, 0));
        s.Append(slotArtwork.DOLocalMoveX(0, .05f).SetEase(Ease.OutCubic));

        slot.Find("name").GetComponent<TextMeshProUGUI>().text = name;
        /*
        slot.Find("player").GetComponentInChildren<TextMeshProUGUI>().text = playernickname;
        slot.Find("iconAndPx").GetComponentInChildren<TextMeshProUGUI>().text = playernumber;
        */
    }

    public Vector2 uiPivot(Sprite sprite)
    {
        Vector2 pixelSize = new Vector2(sprite.texture.width, sprite.texture.height);
        Vector2 pixelPivot = sprite.pivot;
        return new Vector2(pixelPivot.x / pixelSize.x, pixelPivot.y / pixelSize.y);
    }
}
