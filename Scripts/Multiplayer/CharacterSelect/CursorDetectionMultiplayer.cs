using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Unity.Netcode;
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;

public class CursorDetectionMultiplayer : NetworkBehaviour
{

    private GraphicRaycaster gr;
    private PointerEventData pointerEventData = new PointerEventData(null);

    public Transform currentCharacter;

    [SerializeField] private Transform token;
    public bool hasToken;

    private GameObject readyPanel;

    void Start()
    {
        //find ready panel
        var foundCanvasObjects = FindObjectsOfType<CanvasRenderer>(true);   //true = inactive
        foreach (CanvasRenderer canvasObject in foundCanvasObjects)
        {
            if (canvasObject.CompareTag("ReadyPanel"))
            {
                readyPanel = canvasObject.gameObject;
            }
        }
        gr = GetComponentInParent<GraphicRaycaster>();
        CharacterGridMultiplayer.instance.ShowCharacterInSlot(-1);
    }

    void Update()
    {
        if (!IsOwner) { return; }

        if (token == null)
        {
            GameObject[] tokens = GameObject.FindGameObjectsWithTag("Token");
            foreach(GameObject token in tokens)
            {
                if (token.GetComponent<NetworkObject>().IsOwner)
                {
                    this.token = token.transform;
                }
            }
        }

        if (readyPanel.activeInHierarchy && IsHost)
        {            
            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetKeyDown(KeyCode.Z))
            {
                //client rpc to load next scene
                //Transition.LoadLevel("Demo 1", 1.0f, Color.black);
                NetworkManager.Singleton.SceneManager.LoadScene("MultiplayerTest", LoadSceneMode.Single);
                //SceneManager.LoadSceneAsync("MultiplayerTest");
                //LoadSceneClientRpc("MultiplayerTest");
            }
        }

        //CONFIRM
        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (currentCharacter != null)
            {
                TokenFollow(false);
                GameObject.FindWithTag("GameData").GetComponent<GameData>().UpdateCharacter(currentCharacter.GetSiblingIndex());

                //Ready Popup               
                readyPanel.SetActive(true);
                if (RelayManager.Instance.IsHost)
                {                    
                    readyPanel.transform.Find("InfoText").GetComponent<TextMeshProUGUI>().text = "Enter/Esc";
                }
                else
                {
                    readyPanel.transform.Find("InfoText").GetComponent<TextMeshProUGUI>().text = "Waiting for host";
                }
            }
        }

        //CANCEL
        if (Input.GetKeyDown(KeyCode.X) || Input.GetKeyDown(KeyCode.Escape))
        {
            CharacterGridMultiplayer.instance.confirmedCharacter = null;
            TokenFollow(true);

            readyPanel.SetActive(false);
        }

        if (hasToken)
        {
            token.position = transform.position;
        }

        pointerEventData.position = Camera.main.WorldToScreenPoint(transform.position);
        List<RaycastResult> results = new List<RaycastResult>();
        gr.Raycast(pointerEventData, results);

        if (hasToken)
        {
            if (results.Count > 0)
            {
                Transform raycastCharacter = results[0].gameObject.transform;

                if (raycastCharacter != currentCharacter)
                {
                    if (currentCharacter != null)
                    {
                        currentCharacter.Find("selectedBorder").GetComponent<Image>().DOKill();
                        currentCharacter.Find("selectedBorder").GetComponent<Image>().color = Color.clear;
                    }
                    SetCurrentCharacter(raycastCharacter);
                }
            }
            else
            {
                if (currentCharacter != null)
                {
                    currentCharacter.Find("selectedBorder").GetComponent<Image>().DOKill();
                    currentCharacter.Find("selectedBorder").GetComponent<Image>().color = Color.clear;
                    SetCurrentCharacter(null);
                }
            }
        }

    }

    void SetCurrentCharacter(Transform t)
    {
        if (!IsOwner) { return; }

        if (t != null)
        {
            t.Find("selectedBorder").GetComponent<Image>().color = Color.white;
            //t.Find("selectedBorder").GetComponent<Image>().Color(Color.red, .7f).SetLoops(-1);
        }

        currentCharacter = t;

        if (t != null)
        {
            int index = t.GetSiblingIndex();            
            CharacterGridMultiplayer.instance.ShowCharacterInSlot(index);
        }
        else
        {
            CharacterGridMultiplayer.instance.ShowCharacterInSlot(-1);
        }
    }

    void TokenFollow(bool trigger)
    {
        if (!IsOwner) { return; }
        hasToken = trigger;
    }

    public void SetToken(NetworkObject tokenObject)
    {
        token = tokenObject.transform;
    }


    [ClientRpc]
    private void LoadSceneClientRpc(string scene)
    {
        SceneManager.LoadSceneAsync(scene);
    }
}