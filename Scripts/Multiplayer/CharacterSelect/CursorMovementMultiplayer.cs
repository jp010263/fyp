using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class CursorMovementMultiplayer : NetworkBehaviour
{

    [SerializeField] private float speed;
    private Vector3 mousePos;
    private Camera camera;

    private void Start()
    {
        transform.SetParent(GameObject.FindWithTag("Canvas").transform);
        //gameObject.GetComponent<RectTransform>().position.Set(0, 0, 0);
        if (!IsOwner) { return; }
        camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    void Update()
    {
        if (!IsOwner) { return; }

        mousePos = Input.mousePosition;
        transform.position = camera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, camera.nearClipPlane + 1));

        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        transform.position += new Vector3(x, y, 0) * Time.deltaTime * speed;

        Vector3 worldSize = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -worldSize.x, worldSize.x),
            Mathf.Clamp(transform.position.y, -worldSize.y, worldSize.y),
            transform.position.z);


    }
}
