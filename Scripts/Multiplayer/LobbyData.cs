using System.Collections.Generic;
using Unity.Services.Lobbies.Models;

public class LobbyData
{
    private string m_relayJoinCode;
    private string m_sceneName = "CharacterSelectMultiplayer";

    public string SceneName
    {
        get => m_sceneName;
        set => m_sceneName = value;
    }

    public string RelayJoinCode
    {
        get => m_relayJoinCode;
        set => m_relayJoinCode = value;
    }

    public void Initialize(Dictionary<string, DataObject> lobbyData)
    {
        UpdateState(lobbyData);
    }

    public void UpdateState(Dictionary<string, DataObject> lobbyData)
    {

        if (lobbyData.ContainsKey("RelayJoinCode"))
        {
            m_relayJoinCode = lobbyData["RelayJoinCode"].Value;
        }

        if (lobbyData.ContainsKey("SceneName"))
        {
            m_sceneName = lobbyData["SceneName"].Value;
        }
    }

    public Dictionary<string, string> Serialize()
    {
        return new Dictionary<string, string>()
        {
            {"RelayJoinCode", m_relayJoinCode},
            {"SceneName", m_sceneName}
        };
    }
}