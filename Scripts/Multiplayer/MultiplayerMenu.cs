using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MultiplayerMenu : MonoBehaviour
{

    [SerializeField] private GameObject m_selectionMenu;
    [SerializeField] private GameObject m_joinMenu;

    [SerializeField] private TextMeshProUGUI m_codeText;

    [SerializeField] private Button m_hostButton;
    [SerializeField] private Button m_joinButton;

    [SerializeField] private Button m_rejoinButton;
    [SerializeField] private Button m_leaveButton;

    [SerializeField] private Button m_submitCodeButton;
    [SerializeField] private Button m_backButton;

    [SerializeField] private Button m_mainBackButton;

    private async void Start()
    {
        m_hostButton.onClick.AddListener(OnHostClicked);
        m_joinButton.onClick.AddListener(OnJoinClicked);
        m_submitCodeButton.onClick.AddListener(OnSubmitClicked);
        m_backButton.onClick.AddListener(OnBackClicked);
        m_mainBackButton.onClick.AddListener(OnMainBackClicked);

        if (await GameLobbyManager.Instance.HasActiveLobbies())
        {
            Debug.Log("active lobbies");
            m_hostButton.gameObject.SetActive(false);
            m_joinButton.gameObject.SetActive(false);

            m_rejoinButton.gameObject.SetActive(true);
            m_rejoinButton.onClick.AddListener(OnRejoinGameClicked);

            m_leaveButton.gameObject.SetActive(true);
            m_leaveButton.onClick.AddListener(OnLeaveGameClicked);
        }
    }

    private async void OnRejoinGameClicked()
    {
        bool succeeded = await GameLobbyManager.Instance.RejoinGame();
        if (succeeded)
        {
            SceneManager.LoadSceneAsync("Lobby");
        }
    }

    private async void OnLeaveGameClicked()
    {
        bool succeeded = await GameLobbyManager.Instance.LeaveAllLobbies();
        if (succeeded)
        {
            m_hostButton.gameObject.SetActive(true);
            m_joinButton.gameObject.SetActive(true);

            m_rejoinButton.gameObject.SetActive(false);
            m_leaveButton.gameObject.SetActive(false);
        }
    }

    private async void OnHostClicked()
    {
        bool succeeded = await GameLobbyManager.Instance.CreateLobby();
        if (succeeded)
        {
            SceneManager.LoadSceneAsync("Lobby");
        }
    }

    private void OnJoinClicked()
    {
        m_selectionMenu.SetActive(false);
        m_joinMenu.SetActive(true);
    }

    private async void OnSubmitClicked()
    {
        string code = m_codeText.text;
        //last character is blank in the input so is removed
        code = code.Substring(0, code.Length - 1);
        bool succeeded = await GameLobbyManager.Instance.JoinLobby(code);
        if (succeeded)
        {
            SceneManager.LoadSceneAsync("Lobby");
        }
    }

    private void OnBackClicked()
    {
        m_joinMenu.SetActive(false);
        m_selectionMenu.SetActive(true);
    }

    private void OnMainBackClicked()
    {
        SceneManager.LoadSceneAsync("Menu");
    }
}
