using System.Linq;
using System.Threading.Tasks;
using Unity.Services.Relay;
using Unity.Services.Relay.Models;

public class RelayManager : Singleton<RelayManager>
{
    private bool m_isHost = false;
    private string m_joinCode;
    private string m_ip;
    private int m_port;
    private byte[] m_key;
    private byte[] m_connectionData;
    private byte[] m_hostConnectionData;
    private System.Guid m_allocationId;
    private byte[] m_allocationIdBytes;

    public bool IsHost {
        get { return m_isHost; }
    }

    public string GetAllocationId()
    {
        return m_allocationId.ToString();
    }
    public string GetConnectionData()
    {
        return m_connectionData.ToString();
    }

    public async Task<string> CreateRelay(int maxConnection)
    {
        Allocation allocation = await RelayService.Instance.CreateAllocationAsync(maxConnection);
        m_joinCode = await RelayService.Instance.GetJoinCodeAsync(allocation.AllocationId);

        //dtls uses UDP which is suitable for live and real-time data tramsission - tls uses TCP which cannot support.
        RelayServerEndpoint dtlsEndpoint = allocation.ServerEndpoints.First(conn => conn.ConnectionType == "dtls");

        m_ip = dtlsEndpoint.Host;
        m_port = dtlsEndpoint.Port;

        m_allocationId = allocation.AllocationId;
        m_allocationIdBytes = allocation.AllocationIdBytes;
        m_connectionData = allocation.ConnectionData;
        m_key = allocation.Key;


        m_isHost = true;

        return m_joinCode;
    }

    public async Task<bool> JoinRelay(string joinCode)
    {
        m_joinCode = joinCode;
        JoinAllocation allocation = await RelayService.Instance.JoinAllocationAsync(joinCode);

        RelayServerEndpoint dtlsEndpoint = allocation.ServerEndpoints.First(conn => conn.ConnectionType == "dtls");
        m_ip = dtlsEndpoint.Host;
        m_port = dtlsEndpoint.Port;

        m_allocationId = allocation.AllocationId;
        m_allocationIdBytes = allocation.AllocationIdBytes;
        m_connectionData = allocation.ConnectionData;
        m_hostConnectionData = allocation.HostConnectionData;
        m_key = allocation.Key;

        return true;
    }

    public (byte[] allocationId, byte[] key, byte[] connectionData, string dtlsAddress, int dtlsPort) GetHostConnectionInfo()
    {
        return (m_allocationIdBytes, m_key, m_connectionData, m_ip, m_port);
    }

    public (byte[] allocationId, byte[] key, byte[] connectionData, byte[] hostConnectionData, string dtlsAddress, int dtlsPort) GetClientConnectionInfo()
    {
        return (m_allocationIdBytes, m_key, m_connectionData, m_hostConnectionData, m_ip, m_port);
    }
}
