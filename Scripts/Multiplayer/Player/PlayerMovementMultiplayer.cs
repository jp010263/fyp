using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerMovementMultiplayer : NetworkBehaviour
{

    public PlayerControllerMultiplayer controller;

    [SerializeField] private float runSpeed = 400f;
    private float horizontalMove = 0f;
    private bool jump = false;
    private bool punch = false;
    private bool kick = false;
    private bool special = false;

    // Update is called once per frame
    private void Update()
    {

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }

        if (Input.GetKeyDown("left"))
        {
            punch = true;
        }
        if (Input.GetKeyDown("down"))
        {
            kick = true;
        }
        if (Input.GetKeyDown("right"))
        {
            special = true;
        }

    }

    public float HorizontalMove()
    {        
        return horizontalMove;
    }

    public bool Jump()
    {
        if (jump == true)
        {
            jump = false;
            return true;
        }
        return false;
    }

    public bool Punch()
    {
        if (punch == true)
        {
            punch = false;
            return true;
        }
        return false;
    }

    public bool Kick()
        {
        if (kick == true)
        {
            kick = false;
            return true;
        }
        return false;
    }

    public bool Special()
        {
        if (special == true)
        {
            special = false;
            return true;
        }
        return false;
    }
}
