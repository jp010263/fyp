using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Components;

public class PlayerControllerMultiplayer : NetworkBehaviour
{

    private float movespeed = 7.5f;

    [SerializeField] private PlayerMovementMultiplayer pmm;
    [SerializeField] private float m_JumpForce = 40f;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing;
    [SerializeField] private Transform m_GroundPosition;
    [SerializeField] private Transform m_TopPosition;
    [SerializeField] private LayerMask m_GroundMask;

    const float k_GroundRadius = 0.1f;
    private bool m_Grounded;
    const float k_TopRadius = 0.2f;
    private bool m_FacingRight = true;
    private Vector3 m_Velocity = Vector3.zero;

    [SerializeField] private HitColliderMultiplayer hitboxPunch;
    [SerializeField] private HitColliderMultiplayer hitboxKick;
    [SerializeField] private HitColliderMultiplayer hitboxSpecial;
    [SerializeField] private MeshRenderer shield;

    [SerializeField] private ParticleSystem dust;

    [SerializeField] private NetworkAnimator m_Animator;
    [SerializeField] private Rigidbody m_RigidBody;


    private int lives = 2;
    public TextMeshProUGUI livesText;

    private PlayerCanvasMultiplayer pcm;

    // Update is called once per frame
    private void Update()
    {
        if (IsServer && IsLocalPlayer)
        {
            CheckDeath(OwnerClientId);
            Move(pmm.HorizontalMove(), pmm.Jump());

            if (pmm.Punch())
            {
                Punch();
            }
            if (pmm.Kick())
            {
                Kick();
            }
            if (pmm.Special())
            {
                Special();
            }

            SetHitboxInactive();
        }
        if (!IsServer && IsLocalPlayer)
        {
            CheckDeathServerRPC();
            MoveServerRPC(pmm.HorizontalMove(), pmm.Jump());
            UpdateHitboxServerRPC();

            if (pmm.Punch())
            {
                AttackServerRPC(0);
            }
            if (pmm.Kick())
            {
                AttackServerRPC(1);
            }
            if (pmm.Special())
            {
                AttackServerRPC(2);
            }
        }
            Collider[] colliders = Physics.OverlapSphere(m_GroundPosition.position, k_GroundRadius, m_GroundMask);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                {
                    m_Grounded = true;
                }
            }
    }

    private void Move(float move, bool jump)
    {
        //Backward
        if (move < 0 && m_RigidBody.velocity.x > -movespeed)
        {
            m_RigidBody.AddForce(new Vector3(-movespeed * 10, 0, 0));
        }

        //Forward
        if (move > 0 && m_RigidBody.velocity.x < movespeed)
        {
            m_RigidBody.AddForce(new Vector3(movespeed * 10, 0, 0));
        }


        if (m_RigidBody.velocity.x > 1)
        {
            m_RigidBody.AddForce(new Vector3(-movespeed * 5, 0, 0));
        }
        if (m_RigidBody.velocity.x < -1)
        {
            m_RigidBody.AddForce(new Vector3(movespeed * 5, 0, 0));
        }

        if (move > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (move < 0 && m_FacingRight)
        {
            Flip();
        }

        if (m_Grounded && jump)
        {
            m_RigidBody.AddForce(new Vector3(0f, m_JumpForce, 0f));
            m_Animator.Animator.CrossFade("Jump", 0, 0);
            m_Grounded = false;
        }

        if (move != 0 && m_Animator.Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            m_Animator.Animator.CrossFade("Run", 0, 0);
        }
    }

    private void Flip()
    {
        //switch the direction bool
        m_FacingRight = !m_FacingRight;
        if (m_FacingRight)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 120, transform.rotation.z);
        }
        else
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, -50, transform.rotation.z);
        }
    }

    private void Punch()
    {
        m_Animator.Animator.CrossFade("Punch", 0, 0);
        hitboxPunch.setActive();
    }

    private void Kick()
    {
        m_Animator.Animator.CrossFade("Kick", 0, 0);
        hitboxKick.setActive();
    }

    private void Special()
    {
        m_Animator.Animator.CrossFade("Special", 0, 0);
        hitboxSpecial.setActive();
    }

    private void SetHitboxInactive()
    {
        if (!m_Animator.Animator.GetCurrentAnimatorStateInfo(0).IsName("Punch"))
        {
            hitboxPunch.setInactive();
        }
        if (!m_Animator.Animator.GetCurrentAnimatorStateInfo(0).IsName("Kick"))
        {
            hitboxKick.setInactive();
        }
        if (!m_Animator.Animator.GetCurrentAnimatorStateInfo(0).IsName("Special"))
        {
            hitboxSpecial.setInactive();
        }
    }

    private void CheckDeath(ulong clientID)
    {
        //Check if under map then respawn, reset damage if there are lives remaining
        if (gameObject.transform.position.y < -10 || gameObject.transform.position.y > 50)
        {
            if (lives > 0)
            {
                transform.position = GameObject.FindWithTag("Spawner").GetComponent<CharacterSpawnerMultiplayer>().NewSpawn();
                gameObject.GetComponent<PlayerDamageMultiplayer>().ResetDamage();
                lives--;
                if (pcm == null)
                {
                    pcm = GameObject.FindWithTag("Canvas").GetComponent<PlayerCanvasMultiplayer>();
                }
                pcm.ChangeLivesServerRpc(clientID);
                //livesText.text = "<sprite index=0> " + lives.ToString();
            }
            else
            {
                pcm.CheckWinner();
            }
        }
    }

    public int GetLives()
    {
        return lives;
    }

    [ServerRpc]
    private void MoveServerRPC(float move, bool jump)
    {
        Move(move, jump);
    }

    [ServerRpc]
    private void UpdateHitboxServerRPC()
    {
        SetHitboxInactive();
    }

    [ServerRpc]
    private void AttackServerRPC(int attack)
    {
        switch (attack)
        {
            case 0:
                Punch();
                break;
            case 1:
                Kick();
                break;
            case 2:
                Special();
                break;
        }
    }

    [ServerRpc]
    private void CheckDeathServerRPC(ServerRpcParams serverRpcParams = default)
    {
        ulong clientId = serverRpcParams.Receive.SenderClientId;
        CheckDeath(clientId);
    }
}