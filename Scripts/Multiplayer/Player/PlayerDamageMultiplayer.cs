using TMPro;
using Unity.Netcode;
using UnityEngine;

public class PlayerDamageMultiplayer : NetworkBehaviour
{
    public int damagePercentage = 0;
    private TextMeshProUGUI text;
    [SerializeField] private Rigidbody rb;
    private PlayerCanvasMultiplayer pcm;

    public void AddDamage(int damage)
    {        
        if (pcm == null)
        {
            pcm = GameObject.FindWithTag("Canvas").GetComponent<PlayerCanvasMultiplayer>();
        }
        damagePercentage += damage;          
        ulong id = GetComponent<NetworkObject>().OwnerClientId;
        pcm.ChangeDamageServerRpc(damagePercentage, id);
    }

    public void ResetDamage()
    {
        if (pcm == null)
        {
            pcm = GameObject.FindWithTag("Canvas").GetComponent<PlayerCanvasMultiplayer>();
        }
        damagePercentage = 0;
        ulong id = GetComponent<NetworkObject>().OwnerClientId;
        pcm.ChangeDamageServerRpc(damagePercentage, id);
    }

    [ClientRpc]
    public void AddForceClientRpc(ulong objectId, float force)
    {
        if (objectId == GetComponent<NetworkObject>().NetworkObjectId)
        {
            rb.AddForce(force*5, 0, 0);
        }
    }
}
