using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using Unity.Netcode.Components;

public class HitColliderMultiplayer : MonoBehaviour
{
    [SerializeField] private string attackName;
    [SerializeField] private int damage;

    private GameObject owner;

    private bool active = false;

    List<Collider> playersHit = new List<Collider>();

    private void OnTriggerEnter(Collider other)
    {
        if (!active)
        {
            return;
        }
        GameObject col = other.gameObject;
        if (!(col.CompareTag("Player") || col.CompareTag("AI")))
        {
            return;
        }

        //Allow different enemies to be hit (only once each)
        bool isHit = false;
        for (int i = 0; i < playersHit.Count; i++)
        {
            if (playersHit[i] == other)
            {
                isHit = true;
            }
        }
        if (isHit == true)
        {
            return;
        }

        playersHit.Add(other);

        NetworkObject no = col.GetComponent<NetworkObject>();

        ulong id = no.NetworkObjectId;
        AddDamageServerRpc(id, damage);
    }

    [ServerRpc (RequireOwnership = false)]
    private void AddDamageServerRpc(ulong objectId, int damage)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.GetComponent<NetworkObject>().NetworkObjectId == objectId)
            {
                PlayerDamageMultiplayer pdm = player.GetComponent<PlayerDamageMultiplayer>();
                pdm.AddDamage(damage);

                int perc = pdm.damagePercentage;

                float force = damage / 2 * perc;

                if ((transform.position.x - pdm.transform.position.x) < 0)
                {
                    pdm.AddForceClientRpc(objectId, force);
                }
                else if ((transform.position.x - pdm.transform.position.x) > 0)
                {
                    pdm.AddForceClientRpc(objectId, -force);
                }
                return;
            }
        }                  
    }

    public void setActive()
    {
        active = true;
    }

    public void setInactive()
    {
        active = false;
        playersHit.Clear();
    }
}