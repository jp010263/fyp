using UnityEngine;
using Unity.Netcode;
using System.Collections.Generic;

public class CharacterSpawnerMultiplayer : NetworkBehaviour
{
    [SerializeField] private NetworkObject multiplayerPrefab;
    [SerializeField] private Mesh[] characterMeshes;
    [SerializeField] private Transform[] spawnpoints;
    private List<int> usedSpawns = new List<int>();

    public override void OnNetworkSpawn()
    {
        SpawnCharacterServerRpc(GameObject.FindWithTag("GameData").GetComponent<GameData>().selectedCharacter);
        base.OnNetworkSpawn();
    }

    //Spawn character and UI prefab for player
    [ServerRpc(RequireOwnership = false)]
    private void SpawnCharacterServerRpc(int index, ServerRpcParams serverRpcParams = default)
    {        
        ulong clientId = serverRpcParams.Receive.SenderClientId;
        NetworkObject character = Instantiate(multiplayerPrefab);

        character.gameObject.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().sharedMesh = characterMeshes[index];

        bool spawnFound = false;
        while (spawnFound == false)
        {
            int getSpawn = Random.Range(0, spawnpoints.Length);
            if (!usedSpawns.Contains(getSpawn))
            {
                character.transform.position = spawnpoints[getSpawn].position;
                usedSpawns.Add(getSpawn);
                spawnFound = true;
            }
        }
        //character.SpawnWithOwnership(clientId);
        //character.Spawn();
        character.SpawnAsPlayerObject(clientId);

        //Update mesh on clients
        ulong objectId = character.NetworkObjectId;
        UpdateMeshClientRpc(index, objectId);
    }

    [ClientRpc]
    private void UpdateMeshClientRpc(int index, ulong objectId)
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            if (player.GetComponent<NetworkObject>().NetworkObjectId == objectId)
            {
                player.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().sharedMesh = characterMeshes[index];
            }
        }
    }

    private void SpawnCharacter(int index)
    {
        NetworkObject character = Instantiate(multiplayerPrefab);

        bool spawnFound = false;
        while (spawnFound == false)
        {
            int getSpawn = Random.Range(0, spawnpoints.Length);
            if (!usedSpawns.Contains(getSpawn))
            {
                character.transform.position = spawnpoints[getSpawn].position;
                usedSpawns.Add(getSpawn);
                spawnFound = true;
            }
        }
        character.Spawn();
    }

    public Vector3 NewSpawn()
    {
        if (usedSpawns.Count == spawnpoints.Length)
        {
            usedSpawns = new List<int>();
        }
        bool spawnFound = false;
        int getSpawn = Random.Range(0, spawnpoints.Length);
        while (spawnFound == false)
        {
            if (!usedSpawns.Contains(getSpawn))
            {
                usedSpawns.Add(getSpawn);
                spawnFound = true;
            }
            getSpawn = Random.Range(0, spawnpoints.Length);
        }
        return spawnpoints[getSpawn].position;
    }
}
