using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LobbyPlayer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_playerName;
    [SerializeField] private TextMeshProUGUI m_playerReady;

    private LobbyPlayerData m_data;

    private Color m_lobbyColour = new Color(255, 211, 0, 255);
    private Color m_readyColour = Color.green;

    public void SetData(LobbyPlayerData data)
    {
        m_data = data;
        m_playerName.text = m_data.Gamertag;
        gameObject.SetActive(true);

        if (m_data.IsReady)
        {
            //Last character is blank
            if (m_playerReady.text == "In Lobby")
            {
                m_playerReady.text = "Ready";
                m_playerReady.color = m_readyColour; 
            }
        }
        /*
        else
        {
            if (m_playerReady.text[0..^1] == "Ready")
            m_playerReady.text = "In Lobby";
            m_playerReady.color = m_lobbyColour;
        }
        */
    }
}
