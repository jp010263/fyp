using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity.Services.Authentication;
using Unity.Services.Lobbies;
using Unity.Services.Lobbies.Models;
using UnityEngine;

public class LobbyManager : Singleton<LobbyManager>
{

    private Lobby m_lobby;
    private Coroutine m_gameLobbyCoroutine;
    private Coroutine m_refreshLobbyCoroutine;
    private List<string> m_joinedLobbiesId;

    public async Task<bool> HasActiveLobbies()
    {
        m_joinedLobbiesId = await LobbyService.Instance.GetJoinedLobbiesAsync();

        if (m_joinedLobbiesId.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public string GetLobbyCode()
    {
        return m_lobby?.LobbyCode;
    }

    public async Task<bool> CreateLobby(int maxPlayers, bool isPrivate, Dictionary<string, string> data, Dictionary<string, string> lobbyData)
    {
        Dictionary<string, PlayerDataObject> playerData = SerializePlayerData(data);
        Player player = new Player(AuthenticationService.Instance.PlayerId, null, playerData);

        CreateLobbyOptions options = new CreateLobbyOptions()
        {
            Data = SerializeLobbyData(lobbyData),
            IsPrivate = isPrivate,
            Player = player,
        };

        try
        {
            m_lobby = await LobbyService.Instance.CreateLobbyAsync("Lobby", maxPlayers, options);
        }
        catch (System.Exception)
        {
            return false;
        }

        Debug.Log($"Lobby created with lobby id {m_lobby.Id}");

        m_gameLobbyCoroutine = StartCoroutine(GameLobbyCoroutine(m_lobby.Id, 6f));
        m_refreshLobbyCoroutine = StartCoroutine(RefreshLobbyCoroutine(m_lobby.Id, 1f));

        return true;
    }

    private IEnumerator GameLobbyCoroutine(string lobbyId, float waitTime)
    {
        //Run forever
        while (true)
        {
            LobbyService.Instance.SendHeartbeatPingAsync(lobbyId);
            yield return new WaitForSeconds(waitTime);
        }
    }

    private IEnumerator RefreshLobbyCoroutine(string lobbyId, float waitTime)
    {
        //Run forever
        while (true)
        {
            Task<Lobby> task = LobbyService.Instance.GetLobbyAsync(lobbyId);
            yield return new WaitUntil(() => task.IsCompleted);
            Lobby newLobby = task.Result;
            if (newLobby.LastUpdated > m_lobby.LastUpdated)
            {
                m_lobby = newLobby;
                LobbyEvents.OnLobbyUpdated?.Invoke(m_lobby);
            }
            yield return new WaitForSeconds(waitTime);
        }
    }

    private Dictionary<string, PlayerDataObject> SerializePlayerData(Dictionary<string, string> data)
    {
        Dictionary<string, PlayerDataObject> playerData = new Dictionary<string, PlayerDataObject>();

        foreach (var (key, value) in data.Select(x => (x.Key, x.Value)))
        {
            playerData.Add(key, new PlayerDataObject(
                visibility: PlayerDataObject.VisibilityOptions.Member, //Shown only to lobby members
                value: value));
        }

        return playerData;
    }

    public void OnApplicationQuit()
    {
        if (m_lobby != null && m_lobby.HostId == AuthenticationService.Instance.PlayerId)
        {
            LobbyService.Instance.DeleteLobbyAsync(m_lobby.Id);
        }
    }

    public async Task<bool> JoinLobby(string code, Dictionary<string, string> playerData)
    {
        JoinLobbyByCodeOptions options = new JoinLobbyByCodeOptions();
        Player player = new Player(AuthenticationService.Instance.PlayerId, null, SerializePlayerData(playerData));

        options.Player = player;
        try
        {
            m_lobby = await LobbyService.Instance.JoinLobbyByCodeAsync(code, options);
        }
        catch (System.Exception)
        {
            return false;
        }

        m_refreshLobbyCoroutine = StartCoroutine(RefreshLobbyCoroutine(m_lobby.Id, 1f));
        return true;
    }

    public List<Dictionary<string, PlayerDataObject>> GetPlayersData()
    {
        List<Dictionary<string, PlayerDataObject>> data = new List<Dictionary<string, PlayerDataObject>>();

        foreach (Player player in m_lobby.Players)
        {
            data.Add(player.Data);
        }
        return data;
    }

    public async Task<bool> UpdatePlayerData(string playerId, Dictionary<string, string> data, string allocationId = default, string connectionData = default)
    {
        Dictionary<string, PlayerDataObject> playerData = SerializePlayerData(data);

        UpdatePlayerOptions options = new UpdatePlayerOptions()
        {
            Data = playerData,
            AllocationId = allocationId,
            ConnectionInfo = connectionData,
        };
        try
        {
            m_lobby = await LobbyService.Instance.UpdatePlayerAsync(m_lobby.Id, playerId, options);
        }
        catch (System.Exception)
        {
            return false;
        }

        LobbyEvents.OnLobbyUpdated(m_lobby);

        return true;
    }

    public string GetHostId()
    {
        return m_lobby.HostId;
    }

    private Dictionary<string, DataObject> SerializeLobbyData(Dictionary<string, string> data)
    {
        Dictionary<string, DataObject> lobbyData = new Dictionary<string, DataObject>();
        foreach (var (key, value) in data)
        {
            lobbyData.Add(key, new DataObject(
                visibility: DataObject.VisibilityOptions.Member, // Visible only to members of the lobby.
                value: value));
        }

        return lobbyData;
    }

    public async Task<bool> UpdateLobbyData(Dictionary<string, string> data)
    {
        Dictionary<string, DataObject> lobbyData = SerializeLobbyData(data);

        UpdateLobbyOptions options = new UpdateLobbyOptions()
        {
            Data = lobbyData
        };

        try
        {
            m_lobby = await LobbyService.Instance.UpdateLobbyAsync(m_lobby.Id, options);
        }
        catch (System.Exception)
        {
            return false;
        }

        LobbyEvents.OnLobbyUpdated(m_lobby);

        return true;
    }

    public async Task<bool> RejoinLobby()
    {
        try
        {
            m_lobby = await LobbyService.Instance.ReconnectToLobbyAsync(m_joinedLobbiesId[0]);
            LobbyEvents.OnLobbyUpdated(m_lobby);
        } 
        catch (System.Exception)
        {
            return false;
        }

        m_refreshLobbyCoroutine = StartCoroutine(RefreshLobbyCoroutine(m_joinedLobbiesId[0], 1f));
        return true;
    }

    public async Task<bool> LeaveAllLobbies()
    {
        string playerId = AuthenticationService.Instance.PlayerId;
        foreach (string lobbyId in m_joinedLobbiesId)
        {
            try
            {
                await LobbyService.Instance.RemovePlayerAsync(lobbyId, playerId);
            }
            catch (System.Exception)
            {
                return false;
            }
        }
        return true;
    }

}
