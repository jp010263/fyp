using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Unity.Services.Authentication;
using Unity.Services.Lobbies.Models;
using UnityEngine.SceneManagement;

public class GameLobbyManager : Singleton<GameLobbyManager>
{

    private List<LobbyPlayerData> m_lobbyPlayerDatas = new List<LobbyPlayerData>();
    private LobbyPlayerData m_localLobbyPlayerData;
    private LobbyData m_lobbyData;
    private int m_maxNumberOfPlayers = 4;
    private bool m_inGame = false;
    private bool m_wasDisconnected = false;
    private string m_previousRelayCode;


    public bool IsHost => m_localLobbyPlayerData.Id == LobbyManager.Instance.GetHostId();

    private void OnEnable()
    {
        LobbyEvents.OnLobbyUpdated += OnLobbyUpdated;
    }

    private void OnDisable()
    {
        LobbyEvents.OnLobbyUpdated -= OnLobbyUpdated;
    }

    public async Task<bool> HasActiveLobbies()
    {
        return await LobbyManager.Instance.HasActiveLobbies();
    }

    public async Task<bool> CreateLobby()
    {
        m_localLobbyPlayerData = new LobbyPlayerData();
        m_localLobbyPlayerData.Initialize(AuthenticationService.Instance.PlayerId, "Host");  //Host player is player 1

        m_lobbyData = new LobbyData();


        bool succeeded = await LobbyManager.Instance.CreateLobby(m_maxNumberOfPlayers, true, m_localLobbyPlayerData.Serialize(), m_lobbyData.Serialize());
        return succeeded;
    }

    public string GetLobbyCode()
    {
        return LobbyManager.Instance.GetLobbyCode();
    }

    public async Task<bool> JoinLobby(string code)
    {
        m_localLobbyPlayerData = new LobbyPlayerData();
        m_localLobbyPlayerData.Initialize(AuthenticationService.Instance.PlayerId, "Client");  //Client is P2/P3/P4

        bool succeeded = await LobbyManager.Instance.JoinLobby(code, m_localLobbyPlayerData.Serialize());
        return succeeded;
    }

    private async void OnLobbyUpdated(Lobby lobby)
    {
        List<Dictionary<string, PlayerDataObject>> playerData = LobbyManager.Instance.GetPlayersData();
        m_lobbyPlayerDatas.Clear();

        int numberOfPlayersReady = 0;


        foreach(Dictionary<string, PlayerDataObject> data in playerData)
        {
            LobbyPlayerData lobbyPlayerData = new LobbyPlayerData();
            lobbyPlayerData.Initialize(data);

            if (lobbyPlayerData.IsReady)
            {
                numberOfPlayersReady++;
            }

            if(lobbyPlayerData.Id == AuthenticationService.Instance.PlayerId)
            {
                m_localLobbyPlayerData = lobbyPlayerData;
            }

            m_lobbyPlayerDatas.Add(lobbyPlayerData);
        }

        m_lobbyData = new LobbyData();
        m_lobbyData.Initialize(lobby.Data);

        Events.LobbyEvents.OnLobbyUpdated?.Invoke();

        if (numberOfPlayersReady == lobby.Players.Count)
        {
            Events.LobbyEvents.OnLobbyReady?.Invoke();
        }

        if (m_lobbyData.RelayJoinCode != default && !m_inGame)
        {
            if (m_wasDisconnected)
            {
                if (m_lobbyData.RelayJoinCode != m_previousRelayCode)
                {
                    await JoinRelayServer(m_lobbyData.RelayJoinCode);
                    SceneManager.LoadSceneAsync(m_lobbyData.SceneName);
                }
            }
            else
            {
                await JoinRelayServer(m_lobbyData.RelayJoinCode);
                SceneManager.LoadSceneAsync(m_lobbyData.SceneName);
            }
        }
    }

    public List<LobbyPlayerData> GetPlayers()
    {
        return m_lobbyPlayerDatas;
    }

    public async Task<bool> SetPlayerReady()
    {
        m_localLobbyPlayerData.IsReady = true;
        return await LobbyManager.Instance.UpdatePlayerData(m_localLobbyPlayerData.Id, m_localLobbyPlayerData.Serialize());
    }

    public async Task StartGame()
    {
        string joinRelayCode = await RelayManager.Instance.CreateRelay(m_maxNumberOfPlayers);
        m_inGame = true;
        m_lobbyData.RelayJoinCode = joinRelayCode;
        await LobbyManager.Instance.UpdateLobbyData(m_lobbyData.Serialize());

        string allocationId = RelayManager.Instance.GetAllocationId();
        string connectionData = RelayManager.Instance.GetConnectionData();

        m_localLobbyPlayerData.IsReady = false;
        await LobbyManager.Instance.UpdatePlayerData(m_localLobbyPlayerData.Id, m_localLobbyPlayerData.Serialize(), allocationId, connectionData);

        SceneManager.LoadSceneAsync(m_lobbyData.SceneName);
    }

    private async Task<bool> JoinRelayServer(string relayJoinCode)
    {
        m_inGame = true;
        await RelayManager.Instance.JoinRelay(relayJoinCode);

        string allocationId = RelayManager.Instance.GetAllocationId();
        string connectionData = RelayManager.Instance.GetConnectionData();

        m_localLobbyPlayerData.IsReady = false;
        await LobbyManager.Instance.UpdatePlayerData(m_localLobbyPlayerData.Id, m_localLobbyPlayerData.Serialize(), allocationId, connectionData);
        return true;
    }

    public async void GoBackToLobby(bool wasDisconnected)
    {
        m_inGame = false;
        m_wasDisconnected = wasDisconnected;

        if (m_wasDisconnected)
        {
            m_previousRelayCode = m_lobbyData.RelayJoinCode;
        }
        m_localLobbyPlayerData.IsReady = false;
        await LobbyManager.Instance.UpdatePlayerData(m_localLobbyPlayerData.Id, m_localLobbyPlayerData.Serialize());
    }

    public async Task<bool> RejoinGame()
    {
        return await LobbyManager.Instance.RejoinLobby();
    }

    public async Task<bool> LeaveAllLobbies()
    {
        return await LobbyManager.Instance.LeaveAllLobbies();
    }
}
