using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using System.Linq;

public class PlayerCanvasMultiplayer : NetworkBehaviour
{
    private NetworkVariable<int> activeSlots = new NetworkVariable<int>(0);
    [SerializeField] private GameObject[] slots;
    [SerializeField] private WinScreen winScreen;

    [ServerRpc (RequireOwnership = false)]
    public void SetSlotActiveServerRpc(int selectedCharacter, ServerRpcParams serverRpcParams = default)
    {
        if (selectedCharacter > 7) return;
        activeSlots.Value = NetworkManager.Singleton.ConnectedClientsIds.Count;
        slots[serverRpcParams.Receive.SenderClientId].GetComponent<PlayerGameSlot>().SetSlotCharacterServerRpc(selectedCharacter);
    }

    public void SetSlotServer(int selectedCharacter)
    {
        slots[0].GetComponent<PlayerGameSlot>().SetSlotCharacterServer(selectedCharacter);
    }

    [ServerRpc(RequireOwnership = false)]
    public void ChangeDamageServerRpc(int damage, ulong id)
    {
        if (damage < 0) return;        
        slots[id].GetComponent<PlayerGameSlot>().SetSlotDamageServerRpc(damage);
    }

    [ServerRpc(RequireOwnership = false)]
    public void ChangeLivesServerRpc(ulong clientId)
    {
        slots[clientId].GetComponent<PlayerGameSlot>().RemoveLifeServerRpc();
    }

    private void OnEnable()
    {
        activeSlots.OnValueChanged += OnSlotEnabled;
    }

    private void OnDisable()
    {
        activeSlots.OnValueChanged -= OnSlotEnabled;
    }

    private void OnSlotEnabled(int oldActiveSlots, int newActiveSlots)
    {
        for(int i = 0; i < slots.Length; i++)
        {
            if (i < activeSlots.Value)
            {
                slots[i].SetActive(true);
            }
            else
            {
                slots[i].SetActive(false);
            }
        }
    }

    public void CheckWinner()
    {
        List<int> ids = new List<int>();
        
        for (int i = 0; i < activeSlots.Value; i++)
        {
            ids.Add(i);
            if (slots[i].GetComponent<PlayerGameSlot>().GetLives() == 0)
            {
                //update UI
                ids.Remove(i);
            }
        }
        Debug.Log(ids.Count);
        if (ids.Count == 1)
        {
            winScreen.SetWinnerServerRpc(ids[0]+1);
        }
        if (ids.Count == 0)
        {
            winScreen.SetWinnerServerRpc(0);
        }
    }
}
