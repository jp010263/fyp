﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class AI : MonoBehaviour
{

    private float movespeed = 7.5f;
    [SerializeField] private float m_JumpForce = 400f;
    private List<GameObject> enemies;
    private float dist = float.PositiveInfinity;
    private GameObject target = null;
    private Rigidbody m_RigidBody;
    private bool m_FacingRight = true;
    private Animator m_Animator;
    private bool m_Grounded;
    [SerializeField] private Transform m_GroundPosition;
    [SerializeField] private float k_GroundRadius;
    [SerializeField] private LayerMask m_GroundMask;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing;
    [SerializeField] private float k_AttackRange = 0.5f;
    private bool jump = false;

    [SerializeField] private HitCollider hitboxPunch;
    [SerializeField] private HitCollider hitboxKick;
    [SerializeField] private HitCollider hitboxSpecial;

    private int lives = 3;
    public TextMeshProUGUI livesText;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_RigidBody = GetComponent<Rigidbody>();

        List<GameObject> players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        List<GameObject> ai = new List<GameObject>(GameObject.FindGameObjectsWithTag("AI"));
        ai.Remove(gameObject);
        enemies = new List<GameObject>(ai.Union(players).ToList());

        FindTarget();
    }

    // Update is called once per frame
    void Update()
    {
        //Check if under map then respawn, reset damage if there are lives remaining
        if (gameObject.transform.position.y < -10)
        {
            if (lives > 1)
            {
                GameObject.FindWithTag("Spawner").GetComponent<CharacterSpawner>().Respawn(gameObject);
                m_RigidBody.velocity = Vector3.zero;
            }
            if (lives > 0)
            {
                lives--;
                livesText.text = "<sprite index=0> " + (lives).ToString();
                gameObject.GetComponent<PlayerDamage>().ResetDamage();
            } 
        }

        if (m_Grounded && jump)
        {
            m_RigidBody.AddForce(new Vector3(0f, m_JumpForce, 0f));
            m_Animator.CrossFade("Jump", 0, 0);
            //m_Animator.CrossFade("Air", 0.5f, 0);
            jump = false;
        }

        int random = Random.Range(0, 10);
        if (random == 5)
        {
            FindTarget();
        }
    }

    private void FixedUpdate()
    {
        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Punch")) {
            hitboxPunch.setInactive();
        } else
        {
            return;
        }
        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Kick")) {
            hitboxKick.setInactive();
        } else
        {
            return;
        }
        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Special")) {
            hitboxSpecial.setInactive();
        } else
        {
            return;
        }

        m_Grounded = false;

        Collider[] colliders = Physics.OverlapSphere(m_GroundPosition.position, k_GroundRadius, m_GroundMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
            }
        }

        if (target != null)
        {
            if (target.transform.position.x < transform.position.x)
            {
                //Right
                if (m_FacingRight)
                {
                    Flip();
                }

                if ((transform.position - target.transform.position).sqrMagnitude > k_AttackRange)                       
                {                    
                    Move(-1f * movespeed * Time.fixedDeltaTime);                    
                }
                else
                {
                    int random = Random.Range(0, 3);
                    if (random == 0)
                    {
                        Punch();
                    }
                    if (random == 1)
                    {
                        Kick();
                    }
                    if (random == 2)
                    {
                        Special();
                    }
                }
            }
            else
            {
                //Left
                if (!m_FacingRight)
                {
                    Flip();
                }
                if ((transform.position - target.transform.position).sqrMagnitude > k_AttackRange)
                {
                    Move(1f * movespeed * Time.fixedDeltaTime);
                }
                else
                {
                    int random = Random.Range(0, 3);
                    if (random == 0)
                    {
                        Punch();
                    }
                    if (random == 1)
                    {
                        Kick();
                    }
                    if (random == 2)
                    {
                        Special();
                    }
                }
            }
        }
    }

    public void FindTarget()
    {
        dist = float.PositiveInfinity;
        foreach (GameObject enemy in enemies)
        {
            var d = (transform.position - enemy.transform.position).sqrMagnitude;
            if (d < dist)
            {
                target = enemy;
                dist = d;
            }
        }
    }

    public void Move(float move)
    {
        if (move < 0 && m_RigidBody.velocity.x > -movespeed)
        {
            m_RigidBody.AddForce(new Vector3(-movespeed * 10, 0, 0));
        }

        //Forward
        if (move > 0 && m_RigidBody.velocity.x < movespeed)
        {
            m_RigidBody.AddForce(new Vector3(movespeed * 10, 0, 0));
        }

        if (m_RigidBody.velocity.x > 1)
        {
            m_RigidBody.AddForce(new Vector3(-movespeed * 5, 0, 0));
        }
        if (m_RigidBody.velocity.x < -1)
        {
            m_RigidBody.AddForce(new Vector3(movespeed * 5, 0, 0));
        }


        if (move > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (move < 0 && m_FacingRight)
        {
            Flip();
        }
    }

    private void Flip()
    {
        //switch the direction bool
        m_FacingRight = !m_FacingRight;
        if (m_FacingRight)
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, 120, transform.rotation.z);
        }
        else
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, -50, transform.rotation.z);
        }
    }

    public void Punch()
    {
        m_Animator.CrossFade("Punch", 0, 0);
        hitboxPunch.setActive();
    }

    public void Kick()
    {
        m_Animator.CrossFade("Kick", 0, 0);
        hitboxKick.setActive();
    }

    private void Special()
    {
        m_Animator.CrossFade("Special", 0, 0);
        hitboxSpecial.setActive();
    }
    public int GetLives()
    {
        return lives;
    }
}
