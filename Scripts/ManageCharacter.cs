using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ManageCharacter : MonoBehaviour
{
    public Character[] character;
    public bool[] ai;
    public int PlayerNumber = 4;

    private GameObject spawner = null;
    private GameObject winScreen = null;
    List<bool> inGame = new List<bool>();

    private void Awake()
    {
        for (int i = 0; i < PlayerNumber; i++)
        {
            inGame.Add(true);
        }
        DontDestroyOnLoad(this);
        character = new Character[PlayerNumber];
        ai = new bool[PlayerNumber];
    }

    public void ChangeCharacter(int ID, bool ai, Character character)
    {
        this.character[ID] = character;
        this.ai[ID] = ai;
    }

    private void Update()
    {
        if (winScreen == null)
        {
            if (GameObject.FindWithTag("Canvas"))
            {
                winScreen = GameObject.FindWithTag("Canvas").transform.GetChild(1).gameObject;
            }
        }
        if (GameObject.FindWithTag("Spawner"))
        {
            if (spawner == null)
            {
                spawner = GameObject.FindWithTag("Spawner");             
            }
        }

        int count = 0;
        if (!winScreen.activeInHierarchy)
        {
            for (int i = 0; i < PlayerNumber; i++)
            {
                if (spawner.GetComponent<CharacterSpawner>().ReturnLives(i) == 0)
                {
                    //Out of game
                    inGame[i] = false;
                    count++;
                }
            }
        }

        if (count == PlayerNumber - 1)
        {
            //Destroy characters
            spawner.GetComponent<CharacterSpawner>().Despawn();

            //Get winning player
            int winner = inGame.IndexOf(true);

            //Change win screen text
            winScreen.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Player " + (winner + 1) + " Wins";

            //Show win screen
            winScreen.SetActive(true);

        }

    }

}
