using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class transition : MonoBehaviour
{

    public void Transition(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
