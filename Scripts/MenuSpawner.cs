using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSpawner : MonoBehaviour
{
    [SerializeField] GameObject playerModel;
    [SerializeField] Mesh[] meshes;
    void Start()
    {
        int rand = Random.Range(0, meshes.Length);
        GameObject go = Instantiate(playerModel, transform.position, Quaternion.identity);
        go.transform.rotation = Quaternion.Euler(go.transform.rotation.x, go.transform.rotation.y + 180, go.transform.rotation.z);
        go.transform.localScale = new Vector3(3, 3, 3);
        go.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().sharedMesh = meshes[rand];

    }
}
