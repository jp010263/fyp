using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    public void EndTheGame()
    {
        Destroy(GameObject.Find("GameData"));
        Destroy(GameObject.Find("GameLobbyManager"));
        Destroy(GameObject.Find("LobbyManager"));
        Destroy(GameObject.Find("RelayManager"));
        NetworkManager.Singleton.Shutdown();
        Destroy(NetworkManager.Singleton.gameObject);
        SceneManager.LoadSceneAsync("Menu");
    }
}
