using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using TMPro;
using System;

public class PlayerGameSlot : NetworkBehaviour
{
    private NetworkVariable<int> character = new NetworkVariable<int>(0);
    private NetworkVariable<int> damage = new NetworkVariable<int>(0);
    [SerializeField] private Sprite[] sprites;
    [SerializeField] private string[] names;
    private NetworkVariable<int> lives = new NetworkVariable<int>(2);


    [ServerRpc(RequireOwnership = false)]
    public void SetSlotCharacterServerRpc(int selectedCharacter)
    {
        character.Value = selectedCharacter;
    }

    public void SetSlotCharacterServer(int selectedCharacter)
    {
        character.Value = selectedCharacter;
    }

    [ServerRpc(RequireOwnership = false)]
    public void SetSlotDamageServerRpc(int update)
    {
        damage.Value = update;
    }

    [ServerRpc]
    public void RemoveLifeServerRpc()
    {
        lives.Value--;
    }


    private void OnEnable()
    {
        character.OnValueChanged += OnCharacterChanged;
        damage.OnValueChanged += OnDamageChanged;
        lives.OnValueChanged += OnLivesChanged;
    }

    private void OnDisable()
    {
        character.OnValueChanged -= OnCharacterChanged;
        damage.OnValueChanged -= OnDamageChanged;
        lives.OnValueChanged -= OnLivesChanged;
    }

    private void OnCharacterChanged(int oldCharacter, int newCharacter)
    {
        transform.GetChild(0).GetComponent<Image>().sprite = sprites[newCharacter];
        transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = names[newCharacter];
    }

    private void OnDamageChanged(int oldDamage, int newDamage)
    {
        transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = newDamage.ToString() + "%";
    }

    private void OnLivesChanged(int oldLives, int newLives)
    {
        transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = "<sprite index=0> " + (lives.Value + 1).ToString();
    }

    public int GetLives()
    {
        return lives.Value;
    }
}
