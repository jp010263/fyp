using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerSlotMultiplayer : NetworkBehaviour
{
    void Start()
    {
        GetComponent<NetworkObject>().TrySetParent(GameObject.FindWithTag("SlotHolder").transform, false);
        GameObject.FindWithTag("SlotHolder").transform.localScale.Set(1, 1, 1);
    }
}
