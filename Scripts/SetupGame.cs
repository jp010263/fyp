using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class SetupGame : MonoBehaviour
{
    [SerializeField] private GameObject spawner;
    [SerializeField] private GameObject playerCanvas;
    [SerializeField] private GameObject winScreen;
    private void Start()
    {
        PlayerCanvasMultiplayer pcm = playerCanvas.GetComponent<PlayerCanvasMultiplayer>();
        if (NetworkManager.Singleton.IsHost)
        {
            pcm.SetSlotServer(GameObject.FindWithTag("GameData").GetComponent<GameData>().selectedCharacter);
        }
        pcm.SetSlotActiveServerRpc(GameObject.FindWithTag("GameData").GetComponent<GameData>().selectedCharacter);
        winScreen.GetComponent<CanvasRenderer>().SetAlpha(0);
    }
}
