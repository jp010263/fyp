using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscCanvas : MonoBehaviour
{
    [SerializeField] private Canvas escCanvas;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!escCanvas.gameObject.activeInHierarchy)
            {
                escCanvas.gameObject.SetActive(true);
            }
            else
            {
                escCanvas.gameObject.SetActive(false);
            }
            
        }
    }
}
