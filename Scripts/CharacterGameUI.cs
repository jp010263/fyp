﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterGameUI : MonoBehaviour
{
    [SerializeField] private GameObject UIPrefab;
    private GameObject[] charUI;
    [SerializeField] private CharacterSpawner charSpawner;
    private ManageCharacter charManager;

    [SerializeField] private GameObject pauseScreen;
    private bool paused = false;

    private void Awake()
    {
        charManager = GameObject.FindGameObjectWithTag("CharacterManager").GetComponent<ManageCharacter>();
        charUI = new GameObject[charManager.character.Length];

        for (int i = 0; i < charManager.character.Length; i++)
        { 
            charUI[i] = Instantiate(UIPrefab, transform.position, Quaternion.identity);
            charUI[i].transform.parent = gameObject.transform;
            charUI[i].transform.GetChild(0).GetComponent<Image>().sprite = charManager.character[i].characterSprite;
            charUI[i].transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "P" + (i+1);
            charUI[i].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = charManager.character[i].characterName;
            //3rd child changed in SetUIDamageText()
            //4th child changed in Setlives()
        }
        StartCoroutine(SetUIDamageText());
        StartCoroutine(SetLivesText());
    }

    private IEnumerator SetUIDamageText()
    {
        yield return new WaitForSeconds(0.1f); //Relies on CharacterSpawner's start function so needs to be called after
        GameObject[] gos = charSpawner.characterArray();
        for (int i = 0; i < charManager.character.Length; i++)
        {
            gos[i].GetComponent<PlayerDamage>().text = charUI[i].transform.GetChild(3).GetComponent<TextMeshProUGUI>();
        }
    }

    private IEnumerator SetLivesText()
    {
        yield return new WaitForSeconds(0.1f);
        GameObject[] gos = charSpawner.characterArray();
        for (int i = 0; i < charManager.character.Length; i++)
        {
            if (gos[i].GetComponent<PlayerController>())
            {
                gos[i].GetComponent<PlayerController>().livesText = charUI[i].transform.GetChild(4).GetComponent<TextMeshProUGUI>(); 
            }
            if (gos[i].GetComponent<AI>())
            gos[i].GetComponent<AI>().livesText = charUI[i].transform.GetChild(4).GetComponent<TextMeshProUGUI>();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                pauseScreen.SetActive(true);
                paused = true;
            }
            else
            {
                pauseScreen.SetActive(false);
                paused = false;
            }
        }
    }
}
