using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSpawner : MonoBehaviour
{

    private Character[] characterList;
    private bool[] aiList;
    public GameObject[] characters;
    [SerializeField] private Transform[] spawnpoints;
    private int pointer = 0;

    // Start is called before the first frame update
    void Start()
    {
        characterList = GameObject.FindWithTag("CharacterManager").GetComponent<ManageCharacter>().character;
        aiList = GameObject.FindWithTag("CharacterManager").GetComponent<ManageCharacter>().ai;

        characters = new GameObject[characterList.Length];
        for (int i = 0; i < characterList.Length; i++)
        {
            if (aiList[i] == true)
            {
                characters[i] = Instantiate(characterList[i].characterModelAI, spawnpoints[pointer].position, spawnpoints[pointer].rotation);
            }
            else
            {
                characters[i] = Instantiate(characterList[i].characterModel, spawnpoints[pointer].position, spawnpoints[pointer].rotation);
            }
            if (pointer < characterList.Length - 1)
            {
                pointer++;
            }
            else
            {
                pointer = 0;
            }
        }

    }

    public GameObject[] characterArray() {
        return characters;
    }

    public void Respawn(GameObject character)
    {
        foreach (GameObject savedCharacter in characters)
        {
            if (character == savedCharacter)
            {
                character.transform.position = spawnpoints[pointer].position;                
                if (pointer < characterList.Length - 1)
                {
                    pointer++;
                }
                else
                {
                    pointer = 0;
                }
            }
        }
    }
    public int ReturnLives(int num)
    {
        if (characters[num].GetComponent<AI>())
        {
            return characters[num].GetComponent<AI>().GetLives();
        }
        else
        {
            return characters[num].GetComponent<PlayerController>().GetLives();
        }
    }

    public void Despawn()
    {
        foreach(GameObject go in characters)
        {
            Destroy(go);
        }
    }

}
