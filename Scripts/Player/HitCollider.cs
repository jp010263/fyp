using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCollider : MonoBehaviour
{
    [SerializeField] private string attackName;
    [SerializeField] private int damage;

    private GameObject owner;

    private bool active = false;

    List<Collider> playersHit = new List<Collider>();

    private void OnTriggerEnter(Collider other)
    {
        if (!active)
        {
            return;
        }

        GameObject col = other.gameObject;
        if (!(col.CompareTag("Player") || col.CompareTag("AI")))
        {
            return;
        }

        //Allow different enemies to be hit (only once each)
        bool isHit = false;
        for (int i = 0; i < playersHit.Count; i++)
        {
            if (playersHit[i] == other)
            {
                isHit = true;
            }
        }
        if (isHit == true)
        {
            return;
        }

        playersHit.Add(other);

        col.GetComponent<PlayerDamage>().AddDamage(damage);
        int perc = col.GetComponent<PlayerDamage>().damagePercentage;
        //get percentage from col
        float force = damage/2 * perc;

        if ((transform.position.x - col.transform.position.x) < 0)
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(force, 0, 0));
        }
        else if ((transform.position.x - col.transform.position.x) > 0)
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-force, 0, 0));
        }
    }

    public void setActive()
    {
        active = true;
    }

    public void setInactive()
    {
        active = false;
        playersHit.Clear();
    }
}