using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerDamage : MonoBehaviour
{
    public int damagePercentage = 0;
    public TextMeshProUGUI text;

    public void AddDamage(int damage)
    {
        damagePercentage += damage;
        text.text = damagePercentage.ToString() + "%";
    }

    public void ResetDamage()
    {
        damagePercentage = 0;
        text.text = "0%";
    }
}
