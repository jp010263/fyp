﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{

    private float movespeed = 7.5f;
    [SerializeField] private float m_JumpForce = 40f;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing;
    [SerializeField] private Transform m_GroundPosition;
    [SerializeField] private Transform m_TopPosition;
    [SerializeField] private LayerMask m_GroundMask;

    const float k_GroundRadius = 0.1f;
    private bool m_Grounded;
    const float k_TopRadius = 0.2f;
    private Rigidbody m_RigidBody;
    private bool m_FacingRight = true;
    private Animator m_Animator;

    [SerializeField] private HitCollider hitboxPunch;
    [SerializeField] private HitCollider hitboxKick;
    [SerializeField] private HitCollider hitboxSpecial;
    [SerializeField] private MeshRenderer shield;

    private int lives = 3;
    public TextMeshProUGUI livesText;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();
    }

    private void Update()
    {
        //Check if under map then respawn, reset damage if there are lives remaining
        if (gameObject.transform.position.y < -10)
        {
            if (lives > 1)
            {
                GameObject.FindWithTag("Spawner").GetComponent<CharacterSpawner>().Respawn(gameObject);
                m_RigidBody.velocity = Vector3.zero;
            }
            if (lives > 0)
            {
                lives--;
                livesText.text = "<sprite index=0> " + (lives).ToString();
                gameObject.GetComponent<PlayerDamage>().ResetDamage();

            }
        }

        Collider[] colliders = Physics.OverlapSphere(m_GroundPosition.position, k_GroundRadius, m_GroundMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
            }
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {

        //if no animation playing
        if (!(m_Animator.GetCurrentAnimatorClipInfo(0).Length == 0))
        {
            //Idle animation
            if (m_Grounded && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                if (Input.GetAxisRaw("Horizontal") != 0)
                {
                    m_Animator.CrossFade("Run", 0, 0);
                }
            }
        }

        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Punch"))
        {
            hitboxPunch.setInactive();
        }
        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Kick"))
        {
            hitboxKick.setInactive();
        }
        if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Special"))
        {
            hitboxSpecial.setInactive();
        }
    }

    public void Move(float move, bool jump)
    {
        //Backward
        if (move < 0 && m_RigidBody.velocity.x > -movespeed)
        {
            m_RigidBody.AddForce(new Vector3(-movespeed*10, 0, 0));
        }

        //Forward
        if (move > 0 && m_RigidBody.velocity.x < movespeed)
        {
            m_RigidBody.AddForce(new Vector3(movespeed*10, 0, 0));
        }

        if (m_RigidBody.velocity.x > 1)
        {
            m_RigidBody.AddForce(new Vector3(-movespeed * 5, 0, 0));
        }
        if (m_RigidBody.velocity.x < -1)
        {
            m_RigidBody.AddForce(new Vector3(movespeed * 5, 0, 0));
        }


        if (move > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (move < 0 && m_FacingRight)
        {
            Flip();
        }

        if (m_Grounded && jump)
        {
            m_RigidBody.AddForce(new Vector3(0f, m_JumpForce, 0f));
            m_Animator.CrossFade("Jump", 0, 0);
            m_Grounded = false;
            //m_Animator.CrossFade("Air", 0.5f, 0);
        }
    }

    private void Flip()
    {
        //switch the direction bool
        m_FacingRight = !m_FacingRight;
        if (m_FacingRight)
        {         
            transform.rotation = Quaternion.Euler(transform.rotation.x, 120, transform.rotation.z);
        }
        else
        {
            transform.rotation = Quaternion.Euler(transform.rotation.x, -50, transform.rotation.z);
        }
    }

    public void Punch()
    {
        m_Animator.CrossFade("Punch", 0, 0);
        hitboxPunch.setActive();
    }

    public void Kick()
    {
        m_Animator.CrossFade("Kick", 0, 0);
        hitboxKick.setActive();
    }

    public void Special()
    {
        m_Animator.CrossFade("Special", 0, 0);
        hitboxSpecial.setActive();
    }
    public int GetLives()
    {
        return lives;
    }
}